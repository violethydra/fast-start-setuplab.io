/* ==== IMPORT PARAMS ==== */

import { lastRun } from 'gulp';

/* ==== ----- ==== */

/* ==== Sources and directions for files ==== */
const inDev = 'development';
const inDevApps = `${inDev}/components`;
const __node = '/../node_modules';
// dir = `${__dirname}/`;
/* ==== ----- ==== */

const __cfg = {
	list: {
		css: [
			`${__dirname}${__node}/normalize.css/normalize.css`,
			`${__dirname}${__node}/@glidejs/glide/dist/css/glide.core.min.css`,
			`${__dirname}${__node}/@glidejs/glide/dist/css/glide.theme.min.css`,
			
			// `${__dirname}/../node_modules/owl.carousel/dist/assets/owl.carousel.min.css`,
			// `${__dirname}/../node_modules/lite-padding-margin/dist/css-source/lite-padding-margin.min.css`
		],
		map: []
	}
};

/* ==== ----- ==== */

const sinceReplace = x => (`${x}`.replace(/-/gi, ':'));

module.exports = (nameTask, _run, combiner, src, dest, errorConfig, __init) =>
	() => combiner(
		src([...__cfg.list.css, ...__cfg.list.map], { since: lastRun(sinceReplace(nameTask)) }),
		_run.newer(`${inDevApps}/plugins`),
		dest(`${inDevApps}/plugins`)
	).on('error',
		_run.notify.onError((err) => errorConfig(`task: ${nameTask} `, 'ошибка!', err)));
